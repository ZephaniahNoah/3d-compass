-antiauth_procap/anticap*Democracy is a bad system, but so is any state.
-antiauth_antimark_anticap/proauth_promark*Democracy is a good system for maintaining social liberties, and for distributing goods.
-proauth_anticap_antimark/antiauth*Democracy is a bad system of government, but a good way of distributing goods.
+proauth_anticap/antiauth*A vanguard state would be needed to protect the rights of the newly emancipated proletariat.
+proauth_procap/antiauth_anticap*A strong state should be put in place to protect capitalism.
+anticap_antiauth/proauth*Worker emancipation without social liberties would be a great shame.
+antiauth/proauth*Capitalism without social liberties would be a great shame.
+proauth_antimark/antiauth_promark*The state must control the distribution of goods sometimes without the say of the people.
+antiauth_antimark_anticap/procap_promark*Planned distribution of goods should come from democratic decision making from the bottom up, not from the top down.
