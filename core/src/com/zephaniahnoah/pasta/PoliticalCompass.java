package com.zephaniahnoah.pasta;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Mouse;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.zephaniahnoah.pasta.Weight.Direction;

public class PoliticalCompass extends ApplicationAdapter {

	private SpriteBatch batch;
	private Texture compassImage;
	private Texture dotImage;
	private Texture jk;
	private int width;
	private int height;
	private int realWidth = 1;
	private int realHeight = 1;
	private List<String> statements = new ArrayList<String>();
	private List<Weight> weights = new ArrayList<Weight>();
	private boolean[] answers;
	private boolean showCompass = false;
	private BitmapFont font;
	private float textWidth;
	private float textHeight;
	private final String[] statementFiles = new String[] { "distribution.txt", "mixed.txt", "production.txt", "state-power.txt" };
	private int statementIndex = 0;
	private float fractionW = 1;
	private float fractionH = 1;
	private boolean pressed = false;
	private float dx;
	private float dy;
	private int mx;
	private int my;
	private boolean click;
	private double dotX = 0;
	private double dotY = 0;

	public PoliticalCompass(LwjglApplicationConfiguration cfg) {
		width = 1024;
		height = 600;
		DisplayMode ds = new DisplayMode(width, height, 60, 8) {
		};
		cfg.setFromDisplayMode(ds);
		cfg.fullscreen = false;
	}

	@Override
	public void create() {
		batch = new SpriteBatch();
		compassImage = new Texture("AdvancedCompass.png");
		compassImage.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		dotImage = new Texture("dot.png");
		dotImage.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		jk = new Texture("jk.png");
		jk.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("deneane.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 20;
		parameter.characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.!'()>?:*/_+-";
		font = generator.generateFont(parameter);
		generator.dispose();

		// Parse files
		for (String file : statementFiles) {
			FileHandle statementFile = Gdx.files.internal("statements/" + file);
			String[] lines = statementFile.readString().split("\n");
			for (String line : lines) {
				String[] split = line.split("\\*");
				statements.add(split[1]);
				String[] split2 = split[0].split("/");
				char c = split2[0].charAt(0);
				split2[0] = split2[0].substring(1);
				Weight weight = new Weight(c == '+' ? true : false, split2[0].split("_"), split2[1].split("_"));
				weights.add(weight);
			}
		}
		answers = new boolean[statements.size()];
	}

	private void compileAnswers() {
		int count = answers.length;
		for (int i = 0; i < answers.length; i++) {
			Weight weight = weights.get(i);
			List<Direction> dirList = (answers[i] ? weight.directionsAgree : weight.directionsDisagree);
			for (int j = 0; j < dirList.size(); j++) {
				Direction dir = dirList.get(j);
				if (weight.high) {
					dotX += dir.x * 2;
					dotY += dir.y * 2;
					count += 1;
				} else {
					dotX += dir.x;
					dotY += dir.y;
				}
			}
		}
		dotX = dotX / count;
		dotY = dotY / count;
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		if (showCompass) {
			int size = realWidth > realHeight ? realHeight : realWidth;
			size /= 1.2;
			draw(compassImage, realWidth / 2 - size / 2, realHeight / 2 - size / 2, size, size);
			int dotSize = size / 22;
			draw(dotImage, (int) Math.round(realWidth / 2 + (dotX * ((size * .75) / 2))) - dotSize / 2, (int) Math.round(realHeight / 2 + (dotY * ((size * .86) / 2))) - dotSize / 2, dotSize, dotSize);
			size /= 1.6;
			draw(jk, realWidth - size, realHeight - size, size, size);
		} else {
			mx = Mouse.getX();
			my = Mouse.getY();

			Color color = Color.BLACK;
			String text = statements.get(statementIndex);
			reloadTextDimensions(text);
			GlyphLayout layout = textWrapped(text, /* realWidth / 2f - textWidth / 2f */ 0, realHeight / 2f + textHeight / 2f + 15f, color);

			text = "Agree";
			reloadTextDimensions(text);
			dx = realWidth / 2f - textWidth / 2f - 90f;
			dy = realHeight / 2f + textHeight / 2f - layout.height;
			if (collide()) {
				color = Color.LIME;
				if (pressed) {
					color = Color.GREEN;
				}
				if (click) {
					answers[statementIndex] = true;
					statementIndex++;
					if (statementIndex >= statements.size()) {
						showCompass = true;
						compileAnswers();
					}
				}
			} else {
				color = Color.BLACK;
			}
			text(text, dx, dy, color);

			text = "Disagree";
			reloadTextDimensions(text);
			dx = realWidth / 2f - textWidth / 2f + 90f;
			// dy = realHeight / 2f + textHeight / 2f - layout.height;
			if (collide()) {
				color = Color.RED;
				if (pressed) {
					color = Color.SCARLET;
				}
				if (click) {
					answers[statementIndex] = false;
					statementIndex++;
					if (statementIndex >= statements.size()) {
						showCompass = true;
						compileAnswers();
					}
				}
			} else {
				color = Color.BLACK;
			}
			text(text, dx, dy, color);
		}

		batch.end();
	}

	private boolean collide() {
		boolean collide = mx > dx && my < dy && mx < dx + textWidth && my > dy - textHeight;
		if (collide) {
			click = false;
			if (Mouse.isButtonDown(0)) {
				if (!pressed) {
					pressed = true;
					click = true;
				}
			} else {
				pressed = false;
			}
		}
		return collide;
	}

	private void text(String text, float sx, float sy, Color color) {
		font.setColor(color);
		float x = sx * fractionW;
		float y = sy * fractionH;

		font.getData().setScale(fractionW, fractionH);
		font.draw(batch, text, (int) Math.round(x), (int) Math.round(y));
	}

	private GlyphLayout textWrapped(String text, float sx, float sy, Color color) {
		font.setColor(color);
		float x = sx * fractionW;
		float y = sy * fractionH;

		font.getData().setScale(fractionW, fractionH);
		return font.draw(batch, text, (int) Math.round(x), (int) Math.round(y), width - 10, 1, true);
	}

	private void reloadTextDimensions(String text) {
		GlyphLayout glyphLayout = new GlyphLayout();
		glyphLayout.setText(font, text);
		textWidth = glyphLayout.width / fractionW;
		textHeight = glyphLayout.height / fractionH;
	}

	private void draw(Texture tex, int sx, int sy, int wid, int hig) {
		float x = (float) sx * fractionW;
		float y = (float) sy * fractionH;
		float w = (float) wid * fractionW;
		float h = (float) hig * fractionH;

		batch.draw(tex, x, height - y - h, w, h);
	}

	@Override
	public void resize(int w, int h) {
		realWidth = w;
		realHeight = h;
		fractionW = (float) width / (float) w;
		fractionH = (float) height / (float) h;
	}

	@Override
	public void dispose() {
		batch.dispose();
		compassImage.dispose();
	}
}