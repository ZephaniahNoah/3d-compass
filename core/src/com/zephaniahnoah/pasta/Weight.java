package com.zephaniahnoah.pasta;

import java.util.ArrayList;
import java.util.List;

public class Weight {
	public enum Direction {
		proauth(0, -1), //
		procap(1, -.5), //
		promark(1, -procap.y), //
		antiauth(-proauth.x, -proauth.y), //
		anticap(-procap.x, -procap.y), //
		antimark(-promark.x, -promark.y);

		double x;
		double y;

		Direction(double x, double y) {
			this.x = x;
			this.y = y;
		}
	}

	private static final Direction[] directionValues = Direction.values();
	public boolean high;
	public List<Direction> directionsAgree = new ArrayList<Direction>();
	public List<Direction> directionsDisagree = new ArrayList<Direction>();

	public Weight(boolean high, String[] dirsA, String[] dirsD) {
		this.high = high;
		parse(directionsAgree, dirsA);
		parse(directionsDisagree, dirsD);
	}

	private void parse(List<Direction> directions, String[] dirs) {
		for (int i = 0; i < dirs.length; i++) {
			boolean notFound = true;
			for (int j = 0; j < directionValues.length; j++) {
				if (directionValues[j].name().equals(dirs[i])) {
					directions.add(directionValues[j]);
					notFound = false;
					break;
				}
			}
			if (notFound) {
				System.out.println("ERROR! Never found a direction for \"" + dirs[i] + "\"" + " in one of the .txt files...");
			}
		}
	}
}
